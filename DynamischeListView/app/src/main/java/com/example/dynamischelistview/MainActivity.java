package com.example.dynamischelistview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends Activity {

    private ArrayAdapter<String> model;
    private ArrayList<String> elements;

    private ListView listView;
    private EditText editText;
    private int pos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(android.R.id.list);

        editText = findViewById(R.id.input);

        elements = new ArrayList<String>(Arrays.asList("Wort1","Wort2","Wort3","Wort4","Wort5"));

        model = new ArrayAdapter<String>(this,R.layout.checked_text_view_layout, elements);

        listView.setAdapter(model);
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckedTextView checkedTextView = (CheckedTextView)view;

                if(checkedTextView.isChecked()) {
                    pos = i;
                }
            }
        });
    }

    public void add(View view){
        model.add(editText.getText().toString());

        model.notifyDataSetChanged();
    }

    public void sub(View view){
        if(pos != -1){
            Log.e("SUB","pos != -1");
            String item = model.getItem(pos);
            model.remove(item);

            model.notifyDataSetChanged();
        }
        if(pos >= model.getCount()){
            if(model.getCount() > 0){
                pos = 0;
            }
            else {
                pos = -1;
            }
        }
    }
}