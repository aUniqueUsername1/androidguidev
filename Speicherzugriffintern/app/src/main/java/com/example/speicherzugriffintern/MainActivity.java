package com.example.speicherzugriffintern;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private EditText ed;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed = findViewById(R.id.edit_text);
        tv = findViewById(R.id.text);
    }

    public void save(View view) {
        try {
            FileOutputStream out = this.openFileOutput("notes.txt", MODE_APPEND);
            String content = ed.getText().toString();
            out.write(content.getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            tv.setText("save not ok!");
        }
    }

    public void pick(View view) {
        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(this.openFileInput("notes.txt")));
            String content = "";
            String line;
            while ((line = in.readLine()) != null) {

                content += line + "\n";
            }
            ed.setText(content);
            in.close();
        } catch (Exception e) {
            tv.setText("pick not ok!");
        }
    }

    public void mkdir(View view){
        File appdir = this.getDataDir();

        String dirname = ed.getText().toString();
        File nDir = new File(appdir,dirname);
        if(nDir.mkdir()){
            tv.setText("mk dir ok!");
        }else{
            tv.setText("mk dir not ok!");
        }

    }

    public void list(View view){
        File appdir = this.getDataDir();

        String [] content = appdir.list();
        String tvtext = "";
        for(String s : content){
            tvtext += s+"\n";
        }
        tv.setText(tvtext);
    }
}
