package de.hsas.inf.gui_aufgabe2;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class SubActivity extends AppCompatActivity {

    private String cameraPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/Camera";
    private String screenshotPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "/Screenshots";
    private String path;
    private String[] directoryImages;
    private int i = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        Toolbar toolbar2 = findViewById(R.id.toolbar_sub);
        setSupportActionBar(toolbar2);


        Bundle b = this.getIntent().getExtras();
        String selected = b.getString("selection");


        switch(selected) {
            case "Fotos": path = cameraPath;
                break;
            case "Bildschirmfotos": path = screenshotPath;
                break;
            case "Andere Verzeichnisse": path = "";
                break;
        }


        ImageView imageView2 = findViewById(R.id.imageView_sub);
        loadImages();

        imageView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                i++;
                if(i == directoryImages.length) {
                    i = 0;
                }
                loadImages();
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Baut das Menü für die Toolbar
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void loadImages() {
        directoryImages = FileIO.loadImagePathNames(path);
        if(directoryImages == null) {
            Toast.makeText(this, "Nichts drinnen", Toast.LENGTH_SHORT).show();
            return;
        }

        Bitmap bitmap = FileIO.createBitmapFromFile(directoryImages[i]);

        if(bitmap == null) {
            Toast.makeText(this, "Kein Bild vorhanden", Toast.LENGTH_SHORT).show();
            return;
        }

        ImageView imageView2 = findViewById(R.id.imageView_sub);
        imageView2.setImageBitmap(bitmap);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.menu_main) {
            finishAffinity();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
