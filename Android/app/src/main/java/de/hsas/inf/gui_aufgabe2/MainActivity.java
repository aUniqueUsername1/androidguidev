package de.hsas.inf.gui_aufgabe2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    private int PERM_REQUEST_CODE = 42;
    private String selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        checkPermission();

        final ListView listView = findViewById(R.id.listView_Main);
        listView.setItemChecked(-1, true);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        final ArrayList<String> list = new ArrayList<>();
        list.add("Fotos");
        list.add("Bildschirmfotos");
        list.add("Andere Verzeichnisse...");

        listView.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, list));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Button button = findViewById(R.id.button_main);
                button.setEnabled(true);

                selection = list.get(position);
            }
        });
    }

    public void onAuswählenButton (View view) {
        Intent intent = new Intent(this, SubActivity.class);
        intent.putExtra("selection", selection);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Baut das Menü für die Toolbar
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /* PERMISSIONS */
    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERM_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults ){
        if (requestCode == PERM_REQUEST_CODE){
            if((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)){
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.menu_main) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
