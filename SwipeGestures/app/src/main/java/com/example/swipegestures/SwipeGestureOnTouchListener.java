package com.example.swipegestures;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class SwipeGestureOnTouchListener implements View.OnTouchListener {

    private GestureDetector gestureDetector;

    public SwipeGestureOnTouchListener(GestureDetector gestureDetector){
        this.gestureDetector = gestureDetector;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.gestureDetector.onTouchEvent(motionEvent);
    }
}
