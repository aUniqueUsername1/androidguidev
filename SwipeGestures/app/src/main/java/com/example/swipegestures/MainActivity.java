package com.example.swipegestures;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.GestureDetector;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private GestureDetector gestureDetector;

    private TextView textView;

    private char letter = 'A';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) this.findViewById(R.id.textview);

        this.gestureDetector = new GestureDetector(this, new FlingTextGestureListener(this));

        this.textView.setOnTouchListener(new SwipeGestureOnTouchListener(gestureDetector));
    }

    public void links(){
        letter ++;
        if(letter > 'Z'){
            letter = 'A';
        }
        String s = "" + letter;
        textView.setText(s);
    }

    public void rechts() {
        letter--;
        if(letter < 'A'){
            letter = 'Z';
        }
        String s = "" + letter;
        textView.setText(s);
    }
}