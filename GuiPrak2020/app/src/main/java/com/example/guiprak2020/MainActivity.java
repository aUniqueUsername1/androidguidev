package com.example.guiprak2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;



public class MainActivity extends AppCompatActivity {

    private TextView textView;

    private ListView listView;

    private FloatingActionButton floatingActionButton;

    private AppCompatActivity context;

    private ArrayList<String> fileNames = new ArrayList<>();
    private ArrayList<String> fileContent = new ArrayList<>();



    private int fileCount = 0;

    private int lastReplaced = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, fileContent);
       //textView = textView.findViewById(R.id.editText);

        listView = findViewById(R.id.mainList);

        listView.setAdapter(arrayAdapter);

        floatingActionButton = findViewById(R.id.fab_add);

        for(int i = 1; i<6; i++){
        fileNames.add("file"+ i + ".txt");
        fileContent.add(FileIO.readFile(this ,"file" + i + ".txt"));

        }
        if(fileNames.size() == 0){
            FileIO.writeFile(this, "file1.txt", "");

            fileCount = 1;
        }

    }

    public void addNew(View view){
        if(fileCount == 6){
            fileContent.set(lastReplaced, view.getContext().toString());
            FileIO.writeFile(context, fileNames.get(lastReplaced), fileContent.get(lastReplaced));
            lastReplaced++;

        } else if (fileCount < 6){
            fileNames.add("file"+ fileCount + ".txt");
            fileContent.add(""); //FileIO.readFile(context ,"file" + fileCount + ".txt")
            FileIO.writeFile(context, fileNames.get(fileCount), fileContent.get(fileCount));
            fileCount++;
        }
    }


}