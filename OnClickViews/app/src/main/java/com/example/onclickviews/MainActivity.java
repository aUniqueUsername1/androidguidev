package com.example.onclickviews;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.textView = (TextView)findViewById(R.id.label);
    }

    public void writeLabel(View v){
        Button button = (Button) v;

        String labelText = textView.getText().toString();

        labelText += button.getText().toString();

        textView.setText(labelText);
    }
}