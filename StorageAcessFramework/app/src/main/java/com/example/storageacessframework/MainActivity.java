package com.example.storageacessframework;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;



public class MainActivity extends AppCompatActivity {
    private final int SAVE_REQUEST_CODE = 1;
    private final int OPEN_REQUEST_CODE = 2;
    private EditText edit;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit = findViewById(R.id.edit_text);
        text = findViewById(R.id.text);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SAVE_REQUEST_CODE:
                    doSave(data);
                    break;

                case OPEN_REQUEST_CODE:
                    doOpen(data);
                    break;
            }
        }
    }

    public void save(View btnSave) {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, "text.txt");

        startActivityForResult(intent, SAVE_REQUEST_CODE);
    }

    private void doSave(Intent data) {
        try {
            Uri uri = data.getData();
            OutputStream outputStream = getContentResolver().openOutputStream(uri);
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(outputStream));

            printWriter.write(edit.getText().toString());
            printWriter.close();
        } catch (Exception e) {
            text.setText(e.getMessage());
            e.printStackTrace();
        }
    }

    public void pick(View btnOpen){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        startActivityForResult(intent,OPEN_REQUEST_CODE);
    }

    private void doOpen(Intent data) {
        try {
            Uri uri = data.getData();
            InputStream inputStream = getContentResolver().openInputStream(uri);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String content = "";
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content += line + "\n";
            }
            bufferedReader.close();
            edit.setText(content);

        } catch (Exception e) {
            text.setText("Wrong " + e.getMessage());
            e.printStackTrace();
        }
    }
}