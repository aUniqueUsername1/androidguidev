package com.example.guiprak2020;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.style.StyleSpan;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileIO {

    public static ArrayList<String> list (AppCompatActivity context) {
        ArrayList<String> content = new ArrayList<String> ();
        File appdir = context.getDataDir();
        File filesDir = new File(appdir.getAbsolutePath()+"/files");
        FilenameFilter textFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {

                if (name.startsWith("File") && name.endsWith(".txt")) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        String [] filenames = filesDir.list();

        if (filenames != null && filenames.length > 0) {
            for(String f:filenames) {
                content.add(f);
            }
        }
        else {

            try {

                PrintWriter p = new PrintWriter(
                        new OutputStreamWriter(
                          context.openFileOutput("File1.txt", Context.MODE_PRIVATE)));
                p.println("");
                p.close();
                content.add("File1.txt");


            } catch (IOException e) {
                String s = e.getMessage();
                content.add(s);
            }
        }
        return content;
    }

    public static String readFile(AppCompatActivity context, String fName) {
        String content="";
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(context.openFileInput(fName)));
            String line;

            while((line=in.readLine())!= null) {
                content += line + "\n";
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static void writeFile(AppCompatActivity context,String fName, String content){
        try {
            PrintWriter p = new PrintWriter(
                    new OutputStreamWriter(
                            context.openFileOutput(fName, Context.MODE_PRIVATE)));
            Log.e("FILE","vor write content: " + content);
            p.write(content);
            p.flush();
            p.close();
            Log.e("FILE","nach WRITEFILE");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void createFile(AppCompatActivity context, ArrayList<String> content) {
        int n = content.size()+1;
        String fileName = "File"+n+".txt";
        Log.e("FILE" , "new File " + fileName);
        PrintWriter p = null;
        try {
            p = new PrintWriter(
                    new OutputStreamWriter(
                            context.openFileOutput(fileName, Context.MODE_APPEND)));
            p.println("   ");
            p.close();
            content.add(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ArrayList<String> l = list(context);
        Log.e("FILE","Neue Anzahl: "+ l.size());
        for (String s: l) {
            Log.e("FILE","Neu: " +l);
        }

    }

    public static SpannableString readFirstLine(AppCompatActivity context, String fname) {
        String bold = fname + ":\n";
        String standard = "";
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(context.openFileInput(fname)));
            String line;

           if ((standard = in.readLine() ) != null) {
               if(standard.length() > 10) {
                 standard = standard.substring(0,10);
               }
           }
           else {
               standard = "";
           }
           standard+="...";
            in.close();
           Log.e("FILE",standard);
        } catch (IOException e) {
            e.printStackTrace();
        }


        SpannableString result = new SpannableString(bold + standard);
        StyleSpan format = new StyleSpan(Typeface.BOLD);
        result.setSpan(format, 0, bold.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return result;
    }

}
