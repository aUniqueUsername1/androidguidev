package com.example.simpletouch;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.Image;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    private ImageView imageView;

    private Bitmap bitmap;

    private Canvas canvas;

    private Paint paint;

    private float startx = -1;

    private float starty = -1;

    private float stopx = -1;

    private float stopy = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) this.findViewById(R.id.draw);

        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();

        display.getSize(size);

        int width = size.x;

        int height = size.y / 10 * 9;

        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        canvas = new Canvas(bitmap);

        imageView.setImageBitmap(bitmap);

        imageView.setOnTouchListener(this);

        paint = new Paint();

        paint.setStyle(Paint.Style.STROKE);

        paint.setColor(Color.RED);

        paint.setStrokeWidth(3.0f);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.simple_touch_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_bye){
            this.finish();
        }else if(id == R.id.action_red){
            paint.setColor(Color.RED);
            return true;
        }else if(id == R.id.action_blue){
            paint.setColor(Color.BLUE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int radius = 10;

        int action = motionEvent.getAction();

        switch (action){
            case MotionEvent.ACTION_DOWN:
                if(startx == -1 && starty == -1){
                    startx = motionEvent.getX();
                    starty = motionEvent.getY();

                    canvas.drawCircle(startx,starty, 10, paint);
                } else {
                    stopx = startx;
                    stopy = starty;
                    startx = motionEvent.getX();
                    starty = motionEvent.getY();

                    canvas.drawLine(stopx,stopy,startx,starty,paint);
                    canvas.drawCircle(startx,starty,radius,paint);
                }
                imageView.invalidate();
                break;
            default:
                break;
        }
        return true;
    }
}