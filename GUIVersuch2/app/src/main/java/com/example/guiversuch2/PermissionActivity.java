package com.example.guiversuch2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;

public class PermissionActivity extends AppCompatActivity {

    private int PERM_REQUEST_CODE = 42;
    private TextView textfield;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        textfield = findViewById(R.id.textfield);

        checkPermission();
    }

    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERM_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERM_REQUEST_CODE){
            if((grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                testAccess();
            }
        }
    }

    public void testAccess(){
        String ext_path = Environment.getExternalStorageDirectory().getAbsolutePath();

        File access = new File(ext_path);

        String [] content = access.list();

        String sTextField = "";

        if(content != null){
            for(int i=0; i<content.length; i++){
                sTextField += content[i] + "\n";
            }
        }else{
            sTextField = "Nichts gefunden!";
        }
        textfield.setText(sTextField);
    }

}
