package com.example.swipegestures;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class FlingTextGestureListener extends SimpleOnGestureListener {

    private MainActivity mainActivity;

    public FlingTextGestureListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float vX, float vY) {
        float reaction = 20;

        if((motionEvent1.getX() - motionEvent2.getX()) > reaction){
            mainActivity.links();
        }else if((motionEvent2.getX() - motionEvent1.getX()) > reaction){
            mainActivity.rechts();
        }
        return super.onFling(motionEvent1,motionEvent2, vX, vY);
    }
}
